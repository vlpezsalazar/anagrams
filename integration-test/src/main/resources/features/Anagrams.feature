Feature: Get a set of anagrams
  As a user
  I want to get a set of anagrams from my input phrase
  So that the I don't need to look up in a dictionary myself

  Background:
    Given I am a authenticated user

  Scenario: Look for a phrase in an existing dictionary returns a set of anagrams
    When I try to find the anagrams of "blahsaspas" in "integrationDictionary.txt"
    Then I receive a successful response
    And the response contains the following anagrams "blah sas pas", "blah pas sas", "sas blah pas"

  Scenario: Look for a phrase in an existing dictionary returns a empty set of anagrams
    When I try to find the anagrams of "zprtej" in "integrationDictionary.txt"
    Then I receive a successful response
    And the response contains a empty list of anagrams

  Scenario: Look for a phrase in a non existing dictionary returns a error
    When I try to find the anagrams of "someword" in "nonExistingDictionary.txt"
    Then I receive an error response


