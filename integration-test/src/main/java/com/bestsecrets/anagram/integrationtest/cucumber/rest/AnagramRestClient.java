package com.bestsecrets.anagram.integrationtest.cucumber.rest;


import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class AnagramRestClient {

    private WebTarget anagramTarget = ClientBuilder.newClient().target("http://localhost:8080/anagramapp/api/anagram");

    private Response anagramSetResponse;

    public void sendGetAnagramsRequest(String word, String dictionary) {
        anagramSetResponse = anagramTarget.path(dictionary).path(word).request().accept("application/anagrams.v0+json").get();
    }

    public Response getAnagramSetResponse() {
        return anagramSetResponse;
    }
}
