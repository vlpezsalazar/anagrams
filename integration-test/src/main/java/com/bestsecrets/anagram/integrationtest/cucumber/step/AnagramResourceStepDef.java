package com.bestsecrets.anagram.integrationtest.cucumber.step;

import com.bestsecrets.anagram.integrationtest.cucumber.rest.AnagramRestClient;
import cucumber.api.Delimiter;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class AnagramResourceStepDef {

    private final AnagramRestClient anagramRestHelper;

    public AnagramResourceStepDef() {
        this.anagramRestHelper = new AnagramRestClient();
    }

    @Given("^I am a authenticated user$")
    public void iAmAAuthenticatedUser() throws Throwable {}

    @When("^I try to find the anagrams of \"([^\"]*)\" in \"([^\"]*)\"$")
    public void iTryToFindTheAnagramsOfWordInDictionary(String word, String dictionary) throws Throwable {
        anagramRestHelper.sendGetAnagramsRequest(word, dictionary);
    }

    @Then("^I receive a successful response$")
    public void iReceiveASuccessfulResponse() throws Throwable {
        final Response anagramSetResponse = anagramRestHelper.getAnagramSetResponse();

        assertThat(anagramSetResponse.getStatus(), is(OK.getStatusCode()));
    }

    @And("^the response contains the following anagrams (?:([^,]*),?)+$")
    public void theResponseContainsTheFollowingAnagrams(@Delimiter("\"") List<String> anagrams) throws Throwable {
        final Response anagramSetResponse = anagramRestHelper.getAnagramSetResponse();
        final String response = anagramSetResponse.readEntity(String.class);
        assertThat(response, is(notNullValue()));
        anagrams.forEach(anagram -> assertThat(response, containsString(anagram)));
    }

    @Then("^I receive an error response$")
    public void iReceiveAnErrorResponse() throws Throwable {
        final Response anagramSetResponse = anagramRestHelper.getAnagramSetResponse();

        assertThat(anagramSetResponse.getStatus(), is(BAD_REQUEST.getStatusCode()));
    }

    @And("^the response contains a empty list of anagrams$")
    public void theResponseContainsAEmptyListOfAnagrams() throws Throwable {
        final Response anagramSetResponse = anagramRestHelper.getAnagramSetResponse();
        final String response = anagramSetResponse.readEntity(String.class);
        assertThat(response, is(notNullValue()));
        response.contains("anagrams : []");
    }
}
