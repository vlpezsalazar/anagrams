package com.bestsecrets.anagram.engine.test.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class AnagramDictionaryUtils {

    public static final String SEPARATOR_REGEX = "[\\[,\\]\\s]";

    public static InputStream stringToInputStream(String... dictionary) {
        return new ByteArrayInputStream(Arrays.toString(dictionary).getBytes(StandardCharsets.UTF_8));
    }
}
