package com.bestsecrets.anagram.engine.test;

import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Arrays;

public class HamcrestMatchers {
    public static Matcher<AnagramDictionary> containsWords(String... dictionary) {

        return new TypeSafeMatcher<AnagramDictionary>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("iterable over anagram dictionary ");
            }

            @Override
            protected boolean matchesSafely(AnagramDictionary anagramDictionary) {
                return Arrays.stream(dictionary).allMatch(word -> anagramDictionary.contains(word));
            }

            @Override
            protected void describeMismatchSafely(AnagramDictionary item, Description mismatchDescription) {
                mismatchDescription.appendText("does not contains ").appendText(Arrays.asList(dictionary).toString());
            }
        };
    }
}
