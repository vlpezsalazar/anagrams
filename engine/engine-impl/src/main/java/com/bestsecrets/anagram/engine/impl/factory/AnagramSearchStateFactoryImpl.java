package com.bestsecrets.anagram.engine.impl.factory;

import com.bestsecrets.anagram.engine.api.factory.AnagramSearchStateFactory;
import com.bestsecrets.anagram.engine.api.model.AnagramSearchState;
import com.bestsecrets.anagram.engine.impl.model.AnagramSearchStateImpl;

import javax.inject.Named;
import java.util.TreeMap;
import java.util.regex.Pattern;

@Named
public final class AnagramSearchStateFactoryImpl implements AnagramSearchStateFactory<AnagramSearchStateImpl> {

    @Override
    public AnagramSearchStateImpl from(String phrase, String validCharactersRegExp) {
        int phraseLength = phrase.length();
        TreeMap<Character, Integer> characters = new TreeMap<>();
        for (int i = 0; i < phraseLength; i++) {
            if (Pattern.matches(validCharactersRegExp, phrase.subSequence(i, i + 1))) {
                characters.compute(Character.toLowerCase(phrase.charAt(i)), (character, number) -> number == null ? 1 : number + 1);
            }
        }
        return new AnagramSearchStateImpl(characters, "");
    }

    @Override
    public AnagramSearchStateImpl from(AnagramSearchState anagramSearchState, String anagram) {

        TreeMap<Character, Integer> characters = anagramSearchState.getStateCharacters();

        anagram.toLowerCase().chars().forEach(character -> characters.compute((char)character, (character1, integer) -> --integer == 0 ? null : integer));

        String currentPhrase = anagramSearchState.getCurrentPhrase();

        return new AnagramSearchStateImpl(characters,
                currentPhrase + getWordDelimiter(currentPhrase) + anagram);
    }

    private String getWordDelimiter(String currentPhrase) {
        return currentPhrase.isEmpty() ? "" : " ";
    }
}
