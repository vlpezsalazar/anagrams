package com.bestsecrets.anagram.engine.impl.model;

import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import com.bestsecrets.anagram.engine.impl.factory.AnagramSearchStateFactoryImpl;

import java.util.Collection;
import java.util.Collections;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.stream.Stream;

public final class AnagramDictionaryImpl implements AnagramDictionary<AnagramSearchStateImpl> {

    private final NavigableMap<Integer, NavigableSet<String>> wordsAndLengths;
    private final AnagramSearchStateFactoryImpl anagramSearchStateFactory;

    public AnagramDictionaryImpl(AnagramSearchStateFactoryImpl anagramSearchStateFactory, NavigableMap<Integer, NavigableSet<String>> wordsAndLengths) {
        this.anagramSearchStateFactory = anagramSearchStateFactory;
        this.wordsAndLengths = wordsAndLengths;
    }

    @Override
    public Collection<AnagramSearchStateImpl> getNextAnagramSearchSpace(AnagramSearchStateImpl anagramWordSearchState) {
        final int remainingSearchStateCharacters = anagramWordSearchState.numberOfCharacters();

        Stream<NavigableSet<String>> possibleAnagramsToReach = wordsAndLengths.headMap(remainingSearchStateCharacters+1)
                .values().stream();

        return possibleAnagramsToReach.filter(anagramsOfCertainLength ->
                        searchStateAfterCompletionIsValid(remainingSearchStateCharacters, anagramsOfCertainLength) &&
                        anyNodeCanBeCompleted(anagramsOfCertainLength, anagramWordSearchState)
                ).collect(TreeSet::new,
                    (list, anagrams) -> convertReachedAnagramsFromStateToListOfSearchStates(anagramWordSearchState, list, anagrams),
                    (list1, list2) -> {if (list1!=list2) list1.addAll(list2);});
    }

    @Override
    public boolean contains(String word) {
        return wordsAndLengths.getOrDefault(word.length(), Collections.emptyNavigableSet()).stream().anyMatch(word::equals);
    }

    private void convertReachedAnagramsFromStateToListOfSearchStates(AnagramSearchStateImpl anagramWordSearchState, Collection<AnagramSearchStateImpl> list, NavigableSet<String> anagrams) {
        getAnagramsFromState(anagramWordSearchState, anagrams).forEach((String anagram) -> list.add(anagramSearchStateFactory.from(anagramWordSearchState, anagram)));
    }

    private Stream<String> getAnagramsFromState(AnagramSearchStateImpl anagramWordSearchState, NavigableSet<String> anagrams) {
        return anagrams.stream().filter(anagram -> everyCharacterInTheWordIsInTheState(anagramWordSearchState, anagram));
    }

    private boolean anyNodeCanBeCompleted(NavigableSet<String> anagrams, AnagramSearchStateImpl anagramWordSearchState) {
        return anagrams.parallelStream().anyMatch(word -> everyCharacterInTheWordIsInTheState(anagramWordSearchState, word));
    }

    private boolean everyCharacterInTheWordIsInTheState(AnagramSearchStateImpl anagramWordSearchState, String word) {
        return word.chars().distinct().allMatch(x -> numberOfCharacterXInWordIsLessOrEqualThanInState(x, anagramWordSearchState, word));
    }

    private boolean numberOfCharacterXInWordIsLessOrEqualThanInState(int x, AnagramSearchStateImpl anagramWordSearchState, String word) {
        return howMany(x, word) <= anagramWordSearchState.howMany((char) x);
    }

    private long howMany(int x, String word) {
        return word.chars().filter(y -> x == y).count();
    }

    private boolean searchStateAfterCompletionIsValid(int remainingSearchStateCharacters, NavigableSet<String> possibleStates) {
        int remainingSearchStateCharactersAfterCompletion = remainingSearchStateCharacters - possibleStates.first().length();
        return remainingSearchStateCharactersAfterCompletion == 0 || remainingSearchStateCharactersAfterCompletion > 2;
    }
}
