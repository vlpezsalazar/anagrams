package com.bestsecrets.anagram.engine.impl;

import com.bestsecrets.anagram.engine.api.AnagramSearchEngine;
import com.bestsecrets.anagram.engine.impl.factory.AnagramSearchStateFactoryImpl;
import com.bestsecrets.anagram.engine.impl.model.AnagramDictionaryImpl;
import com.bestsecrets.anagram.engine.impl.model.AnagramSearchStateImpl;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.TreeSet;
import java.util.function.Predicate;

@Named
public class AnagramSearchEngineImpl implements AnagramSearchEngine<AnagramSearchStateImpl, AnagramDictionaryImpl> {

    private final AnagramSearchStateFactoryImpl anagramSearchStateFactory;

    @Inject
    public AnagramSearchEngineImpl(AnagramSearchStateFactoryImpl anagramSearchStateFactory) {
        this.anagramSearchStateFactory = anagramSearchStateFactory;
    }

    public Collection<String> lookForAnagrams(AnagramDictionaryImpl anagramDictionary, String phrase, String validCharacterRegExp) {
        AnagramSearchStateImpl rootState = anagramSearchStateFactory.from(phrase, validCharacterRegExp);
        Collection<String> anagrams = new TreeSet<>();

        TreeSet<AnagramSearchStateImpl> states = new TreeSet<>();
        states.add(rootState);

        while (!states.isEmpty()) {
            AnagramSearchStateImpl currentState = states.pollFirst();
            if (currentState.isComplete()) {
                anagrams.add(currentState.getCurrentPhrase());
                removeSimilarStates(states, anagrams);
            } else {

                final Collection<AnagramSearchStateImpl> nextAnagramSearchSpace = anagramDictionary.getNextAnagramSearchSpace(currentState);

                if (nextAnagramSearchSpace.isEmpty()) {
                    states.removeIf(state -> currentState.getStateCharacters().equals(state.getStateCharacters()));
                } else {
                    removeSimilarStates(nextAnagramSearchSpace, anagrams);
                    states.addAll(nextAnagramSearchSpace);
                }
            }
        }
        return anagrams;
    }

    public void removeSimilarStates(Collection<AnagramSearchStateImpl> states, Collection<String> anagrams) {
        for (String word : anagrams) {
            final String[] words = word.split(" ");
            for (int i = 0; i < words.length; i++) {
                states.removeIf(stateContainsWord(words[i], i, words.length));
            }
        }
    }

    public Predicate<AnagramSearchStateImpl> stateContainsWord(String word, int position, int length) {
        return state -> {
            final String currentPhrase = state.getCurrentPhrase();
            if (currentPhrase.matches("^(.+ )?" + word + "( .+)?$")) {
                final String[] stateWords = currentPhrase.split(" ");
                if (stateWords.length < length) {
                    for (int i = 0; i < stateWords.length; i++) {
                        if (stateWords[i].equals(word) && i != position) {
                            return true;
                        }
                    }
                }
            }
            return false;
        };

    }

    private void print(String format, Object... values) {
        System.out.println(String.format(format, values));
    }
}
