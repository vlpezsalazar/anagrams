package com.bestsecrets.anagram.engine.impl.model;

import com.bestsecrets.anagram.engine.api.model.AnagramSearchState;

import java.util.TreeMap;


public final class AnagramSearchStateImpl implements AnagramSearchState<AnagramSearchStateImpl> {

    private final TreeMap<Character, Integer> characters;
    private final String currentPhrase;

    public AnagramSearchStateImpl(TreeMap<Character, Integer> characters, String currentPhrase) {
        this.characters = characters;
        this.currentPhrase = currentPhrase;
    }

    @Override
    public String getCurrentPhrase() {
        return currentPhrase;
    }

    @Override
    public TreeMap<Character, Integer> getStateCharacters() {
        TreeMap<Character, Integer> charactersCopy = new TreeMap<>();
        charactersCopy.putAll(characters);
        return charactersCopy;
    }

    @Override
    public boolean isComplete() {
        return characters.isEmpty();
    }

    @Override
    public int numberOfCharacters() {
        return characters.values().stream().reduce(0, Integer::sum);
    }

    @Override
    public Integer howMany(Character character) {
        return characters.getOrDefault(character, 0);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;

        if (obj == null || !obj.getClass().equals(AnagramSearchStateImpl.class)) return false;

        return characters.equals(((AnagramSearchStateImpl) obj).characters) &&
                currentPhrase.equals(((AnagramSearchStateImpl) obj).getCurrentPhrase());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((characters == null) ? 0 : characters.hashCode())
                + ((currentPhrase == null) ? 0 : currentPhrase.hashCode());
        return result;
    }

    @Override
    public int compareTo(AnagramSearchStateImpl comparableState) {
        int compare = numberOfCharacters() - comparableState.numberOfCharacters();

        if (compare != 0) return compare;

        if (compare == 0){
            compare = getCurrentPhrase().compareTo(comparableState.getCurrentPhrase());
        }

        return compare;
    }

    @Override
    public String toString() {
        return characters.toString() + "(" + currentPhrase + ")";
    }
}
