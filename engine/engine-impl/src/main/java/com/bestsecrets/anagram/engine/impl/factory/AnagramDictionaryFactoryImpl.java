package com.bestsecrets.anagram.engine.impl.factory;

import com.bestsecrets.anagram.engine.api.factory.AnagramDictionaryFactory;
import com.bestsecrets.anagram.engine.impl.model.AnagramDictionaryImpl;

import javax.inject.Named;
import java.io.InputStream;
import java.util.NavigableSet;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

@Named
public final class AnagramDictionaryFactoryImpl implements AnagramDictionaryFactory<AnagramDictionaryImpl> {

    @Override
    public AnagramDictionaryImpl load(InputStream dictionary, String separatorRegExp) {
        TreeMap<Integer, NavigableSet<String>> possibleAnagramsFromHere = new TreeMap<>();
        try (Scanner scanner = new Scanner(dictionary).useDelimiter(separatorRegExp)) {

            while (scanner.hasNext()) {
                String word = scanner.next().toLowerCase();
                if (word.length() > 2) {
                    possibleAnagramsFromHere.computeIfAbsent(word.length(), key -> new TreeSet<>()).add(word);
                }

            }
        }

        return new AnagramDictionaryImpl(new AnagramSearchStateFactoryImpl(), possibleAnagramsFromHere);
    }

}
