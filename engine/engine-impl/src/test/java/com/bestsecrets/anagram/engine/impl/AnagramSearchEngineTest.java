package com.bestsecrets.anagram.engine.impl;

import com.bestsecrets.anagram.engine.api.AnagramSearchEngine;
import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import com.bestsecrets.anagram.engine.impl.factory.AnagramDictionaryFactoryImpl;
import com.bestsecrets.anagram.engine.impl.factory.AnagramSearchStateFactoryImpl;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collection;

import static com.bestsecrets.anagram.engine.api.factory.AnagramSearchStateFactory.ALPHABETIC_CHARACTERS;
import static com.bestsecrets.anagram.engine.test.utils.AnagramDictionaryUtils.SEPARATOR_REGEX;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class AnagramSearchEngineTest {

    private static AnagramSearchEngine anagramSearchEngine;
    private static AnagramDictionary anagramDictionary;
    private long a;
    private long b;

    @BeforeClass
    public static void setup() {
        anagramDictionary = new AnagramDictionaryFactoryImpl().load(ClassLoader.getSystemResourceAsStream("anagramDic.txt"), SEPARATOR_REGEX);
        anagramSearchEngine = new AnagramSearchEngineImpl(new AnagramSearchStateFactoryImpl());
    }

    @Test
    public void lookForAnagrams_shouldReturnAnOrderedCombinationOfAnagramsInDictionary_forBestSecrets() throws Exception {
        //given
        final String phrase = "Best Secret";

        //when
        a = System.currentTimeMillis();
        Collection<String> anagrams = anagramSearchEngine.lookForAnagrams(anagramDictionary, phrase, ALPHABETIC_CHARACTERS);
        b = System.currentTimeMillis();

        //then
        assertThat(anagrams, contains("beets crest", "beret sects", "berets sect", "beset crest", "bests crete",
                "bests erect", "better cess", "betters sec", "crests beet", "erects best", "erects bets",
                "erst bet sec", "rest bet sec", "secret best", "secret bets", "secrets bet"));
    }

    @Test
    public void lookForAnagrams_shouldReturnAnOrderedCombinationOfAnagramsInDictionary_forITCrowd() throws Exception {
        //given
        final String phrase = "IT-Crowd";

        //when
        a = System.currentTimeMillis();
        Collection<String> anagrams = anagramSearchEngine.lookForAnagrams(anagramDictionary, phrase, ALPHABETIC_CHARACTERS);
        b = System.currentTimeMillis();

        //then
        assertThat(anagrams, contains("cord wit", "dirt cow", "word tic", "writ cod", "writ doc"));
    }

    @After
    public void printTime() {
        System.out.printf("Time looking for anagrams of : %d ms%n", b - a);
    }

}