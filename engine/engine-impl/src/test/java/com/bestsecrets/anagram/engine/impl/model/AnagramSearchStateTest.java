package com.bestsecrets.anagram.engine.impl.model;

import org.junit.Before;
import org.junit.Test;

import java.util.TreeMap;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class AnagramSearchStateTest {

    private AnagramSearchStateImpl anagramSearchState;
    private TreeMap<Character, Integer> characters;

    @Before
    public void setUp(){
        characters = new TreeMap<>();
        characters.put('a', 2);
        characters.put('e', 1);
        characters.put('i', 1);
        characters.put('o', 1);
        characters.put('u', 1);

        anagramSearchState = new AnagramSearchStateImpl(characters, "word");
    }

    @Test
    public void numberOfCharacters_shouldReturnTheTotalNumberOfCharactersInTheState() throws Exception {
        assertThat(anagramSearchState.numberOfCharacters(), is(6));
    }

    @Test
    public void howMany_shouldReturnTheNumberOfAGivenCharacter() throws Exception {
        assertThat(anagramSearchState.howMany('a'), is(2));
    }

    @Test
    public void howMany_shouldReturn0_whenCharacterIsNotPresent() throws Exception {
        assertThat(anagramSearchState.howMany('z'), is(0));
    }

    @Test
    public void getStateCharacters_shouldReturnACopyOfTheCharacterMapInTheState() throws Exception {
        TreeMap<Character, Integer> stateCharactersClone = anagramSearchState.getStateCharacters();
        assertThat(stateCharactersClone, equalTo(characters));
    }

    @Test
    public void getStateCharacters_shouldNotReturnTheSameInternalMapInTheState() throws Exception {
        TreeMap<Character, Integer> stateCharactersClone = anagramSearchState.getStateCharacters();
        assertThat(stateCharactersClone, not(sameInstance(characters)));
    }

    @Test
    public void getCurrentPhrase_shouldReturnEmptyString_whenStateIsClean() throws Exception {
        assertThat(anagramSearchState.getCurrentPhrase(), is("word"));
    }

}