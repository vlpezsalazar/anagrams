package com.bestsecrets.anagram.engine.impl.factory;

import com.bestsecrets.anagram.engine.api.factory.AnagramSearchStateFactory;
import com.bestsecrets.anagram.engine.api.model.AnagramSearchState;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class AnagramSearchEngineStateFactoryTest {

    private AnagramSearchStateFactory anagramSearchStateFactory;

    @Before
    public void setup(){
        anagramSearchStateFactory = new AnagramSearchStateFactoryImpl();
    }

    @Test
    public void from_shouldCreateAStateIncludingOnlyValidCharacters_whenStringContainsSomethingElse() throws Exception {
        //given
        final String phrase = "-pd:$4%";
        final String validCharacters = "[a-zA-Z]";

        //when
        AnagramSearchState state = anagramSearchStateFactory.from(phrase, validCharacters);


        //then
        assertThat(state.numberOfCharacters(), is(2));
        assertThat(state.howMany('p'), is(1));
        assertThat(state.howMany('d'), is(1));
    }

    @Test
    public void from_shouldRemoveTheCharactersOfTheIncludedAnagram() throws Exception {
        //given
        final AnagramSearchState parentState = anagramSearchStateFactory.from("Best Secrets", "[a-zA-Z]");

        //when
        AnagramSearchState childState = anagramSearchStateFactory.from(parentState, "best");

        //then
        assertThat(childState.howMany('b'), is(0));
        assertThat(childState.howMany('c'), is(1));
        assertThat(childState.howMany('r'), is(1));
        assertThat(childState.howMany('s'), is(2));
        assertThat(childState.howMany('e'), is(2));
        assertThat(childState.howMany('t'), is(1));
    }

    @Test
    public void from_shouldIncludeTheAnagramInTheCurrentPhraseOfTheState() throws Exception {
        //given
        final AnagramSearchState parentState = anagramSearchStateFactory.from("Best Secrets", "[a-zA-Z]");

        //when
        AnagramSearchState childState = anagramSearchStateFactory.from(parentState, "best");

        //then
        assertThat(childState.getCurrentPhrase(), is("best"));
    }

    @Test
    public void from_shouldCreateACompleteState_whenParentStateContainsTheSameCharactersOfTheWords() throws Exception {
        //given
        final AnagramSearchState parentState = anagramSearchStateFactory.from("salia", "[a-zA-Z]");

        //when
        AnagramSearchState childState = anagramSearchStateFactory.from(parentState, "salia");

        //then
        assertThat(childState.isComplete(), is(true));
    }


}