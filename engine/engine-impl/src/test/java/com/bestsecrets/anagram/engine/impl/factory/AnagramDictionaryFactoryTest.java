package com.bestsecrets.anagram.engine.impl.factory;

import com.bestsecrets.anagram.engine.api.factory.AnagramDictionaryFactory;
import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import com.bestsecrets.anagram.engine.test.HamcrestMatchers;
import org.junit.Before;
import org.junit.Test;

import static com.bestsecrets.anagram.engine.test.utils.AnagramDictionaryUtils.SEPARATOR_REGEX;
import static com.bestsecrets.anagram.engine.test.utils.AnagramDictionaryUtils.stringToInputStream;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

public class AnagramDictionaryFactoryTest {

    private AnagramDictionaryFactory anagramDictionaryFactory;

    @Before
    public void setup(){
        anagramDictionaryFactory = new AnagramDictionaryFactoryImpl();
    }

    @Test
    public void load_shouldCreateAPopulatedAnagramDictionary_fromArrayString(){
        //given
        final String[] dictionary = {"isa", "lisa", "mona", "mano"};

        //when
        AnagramDictionary anagramDictionary = anagramDictionaryFactory.load(stringToInputStream(dictionary), SEPARATOR_REGEX);

        //then
        assertThat(anagramDictionary, is(notNullValue()));
        assertThat(anagramDictionary, HamcrestMatchers.containsWords(dictionary));
    }

}
