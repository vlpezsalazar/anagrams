package com.bestsecrets.anagram.engine.api.model;

import java.util.Collection;

public interface AnagramDictionary<AnagramSearchStateType extends AnagramSearchState> {

    Collection<AnagramSearchStateType> getNextAnagramSearchSpace(AnagramSearchStateType anagramWordSearchState);

    boolean contains(String word);
}
