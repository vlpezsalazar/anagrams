package com.bestsecrets.anagram.engine.api;

import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import com.bestsecrets.anagram.engine.api.model.AnagramSearchState;

import java.util.Collection;

public interface AnagramSearchEngine<AnagramSearchStateType extends AnagramSearchState,
                                 AnagramDictionaryType extends AnagramDictionary<AnagramSearchStateType>> {

    Collection<String> lookForAnagrams(AnagramDictionaryType anagramDictionary, String phrase, String validCharacterRegExp);
}
