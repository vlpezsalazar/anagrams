package com.bestsecrets.anagram.engine.api.factory;

import com.bestsecrets.anagram.engine.api.model.AnagramSearchState;

public interface AnagramSearchStateFactory<AnagramSearchStateType extends AnagramSearchState> {
    String ALPHABETIC_CHARACTERS = "[a-zA-Z]";

    AnagramSearchStateType from(String phrase, String validCharactersRegExp);
    AnagramSearchStateType from(AnagramSearchState anagramSearchState, String anagram);
}
