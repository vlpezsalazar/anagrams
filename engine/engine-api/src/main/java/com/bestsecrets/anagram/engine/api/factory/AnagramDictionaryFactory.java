package com.bestsecrets.anagram.engine.api.factory;

import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;

import java.io.InputStream;

public interface AnagramDictionaryFactory<AnagramDictionaryType extends AnagramDictionary> {
    AnagramDictionaryType load(InputStream byteArrayInputStream, String separatorRegExp);
}
