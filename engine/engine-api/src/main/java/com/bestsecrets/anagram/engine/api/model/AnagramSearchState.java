package com.bestsecrets.anagram.engine.api.model;

import java.util.TreeMap;

public interface AnagramSearchState<AnagramSearchStateType extends AnagramSearchState> extends Comparable<AnagramSearchStateType>{

    TreeMap<Character, Integer> getStateCharacters();

    String getCurrentPhrase();

    Integer howMany(Character character);

    boolean isComplete();

    int numberOfCharacters();

}
