package com.bestsecrets.anagram.rest.configuration;

import com.bestsecrets.anagram.rest.filter.RestResponseFilter;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/anagramapp/api")
@Lazy
public class JerseyConfig extends ResourceConfig {
 
    public JerseyConfig() {
        register(RequestContextFilter.class);
        register(RestResponseFilter.class);
        packages("com.bestsecrets.anagram.rest.resource");
        register(LoggingFeature.class);
    }

}