package com.bestsecrets.anagram.rest.service.tasks;

import com.bestsecrets.anagram.engine.api.AnagramSearchEngine;
import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import com.bestsecrets.anagram.rest.dto.AnagramSetResponse;
import com.bestsecrets.anagram.rest.exceptions.DictionaryNotFoundException;
import com.bestsecrets.anagram.rest.service.api.DictionaryLoaderService;

import javax.ws.rs.container.AsyncResponse;

import static com.bestsecrets.anagram.engine.api.factory.AnagramSearchStateFactory.ALPHABETIC_CHARACTERS;

public class AnagramSearchTask implements Runnable {
    private final DictionaryLoaderService dictionaryLoaderService;
    private final AnagramSearchEngine anagramSearchEngine;
    private final String dictionary;
    private final String phrase;
    private final AsyncResponse asyncResponse;

    public AnagramSearchTask(DictionaryLoaderService dictionaryLoaderService, AnagramSearchEngine anagramSearchEngine, String dictionary, String phrase, AsyncResponse asyncResponse) {
        this.dictionaryLoaderService = dictionaryLoaderService;
        this.anagramSearchEngine = anagramSearchEngine;
        this.dictionary = dictionary;
        this.phrase = phrase;
        this.asyncResponse = asyncResponse;
    }

    @Override
    public void run() {
        try {
            final AnagramDictionary anagramDictionary = dictionaryLoaderService.load(dictionary);
            asyncResponse.resume(AnagramSetResponse.of(anagramSearchEngine.lookForAnagrams(anagramDictionary, phrase, ALPHABETIC_CHARACTERS)));
        } catch (DictionaryNotFoundException e) {
            asyncResponse.resume(e);
        }
    }
}
