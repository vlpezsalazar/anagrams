package com.bestsecrets.anagram.rest.service.api;

import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;

@Component
public interface AnagramSearchService {

    Runnable lookupAnagramsAsync(String dictionary, String phrase, AsyncResponse asyncResponse);
}
