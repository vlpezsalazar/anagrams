package com.bestsecrets.anagram.rest.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.IOException;

public class DictionaryNotFoundException extends WebApplicationException {
    public DictionaryNotFoundException(IOException e, Response.Status badRequest) {
        super(e, badRequest);
    }
}
