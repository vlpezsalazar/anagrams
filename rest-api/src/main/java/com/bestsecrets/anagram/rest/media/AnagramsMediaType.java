package com.bestsecrets.anagram.rest.media;

public class AnagramsMediaType {
    public static final String ANAGRAMS_JSON = "application/anagrams.v0+json";
}
