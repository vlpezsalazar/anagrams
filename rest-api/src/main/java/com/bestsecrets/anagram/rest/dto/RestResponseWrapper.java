package com.bestsecrets.anagram.rest.dto;

import org.springframework.hateoas.ResourceSupport;


public class RestResponseWrapper extends ResourceSupport {
    private Object content;

    public RestResponseWrapper(Object content) {
        this.content = content;
    }

    public Object getContent(){
        return content;
    }

}
