package com.bestsecrets.anagram.rest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.Collection;

@Value.Immutable
@JsonDeserialize(builder = ImmutableAnagramSetResponse.Builder.class)
public abstract class AnagramSetResponse {
    public abstract Collection<String> anagrams();

    public static AnagramSetResponse of(Collection<String> anagrams){
        return ImmutableAnagramSetResponse.builder().anagrams(anagrams).build();
    }

}
