package com.bestsecrets.anagram.rest.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class NotValidException extends WebApplicationException {
    public NotValidException(){
        super(Response.Status.METHOD_NOT_ALLOWED);
    }

}
