package com.bestsecrets.anagram.rest.service.api;

import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public interface DictionaryLoaderService {

    AnagramDictionary load(String dictionary);
    List<String> listDictionaries() throws IOException;

}
