package com.bestsecrets.anagram.rest.service.impl;

import com.bestsecrets.anagram.engine.api.factory.AnagramDictionaryFactory;
import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import com.bestsecrets.anagram.rest.exceptions.DictionaryNotFoundException;
import com.bestsecrets.anagram.rest.service.api.DictionaryLoaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class DictionaryLoaderServiceImpl implements DictionaryLoaderService {

    static final String SEPARATOR_REGEX = "[\\[,\\]\\s]";
    static final String DICTIONARIES_PATH = "classpath:dictionaries/";
    private static final Logger LOGGER = LoggerFactory.getLogger(DictionaryLoaderServiceImpl.class);

    private final AnagramDictionaryFactory anagramDictionaryFactory;
    private final ResourceLoader resourceLoader;

    @Autowired
    public DictionaryLoaderServiceImpl(AnagramDictionaryFactory anagramDictionaryFactory, ResourceLoader resourceLoader) {
        this.anagramDictionaryFactory = anagramDictionaryFactory;
        this.resourceLoader = resourceLoader;
    }

    public AnagramDictionary load(String dictionary) {
        try {

            final Resource resource = resourceLoader.getResource(DICTIONARIES_PATH + dictionary);
            return anagramDictionaryFactory.load(resource.getInputStream(), SEPARATOR_REGEX);

        } catch (IOException e) {
            LOGGER.warn("", e);
            throw new DictionaryNotFoundException(e, Response.Status.BAD_REQUEST);
        }
    }

    public List<String> listDictionaries() throws IOException {
        return Arrays.asList(resourceLoader.getResource(DICTIONARIES_PATH).getFile().list());
    }

}
