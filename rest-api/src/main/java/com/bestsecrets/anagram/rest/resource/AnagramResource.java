package com.bestsecrets.anagram.rest.resource;

import com.bestsecrets.anagram.rest.exceptions.NotValidException;
import com.bestsecrets.anagram.rest.filter.HATEOAS;
import com.bestsecrets.anagram.rest.service.api.AnagramSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.bestsecrets.anagram.rest.media.AnagramsMediaType.ANAGRAMS_JSON;
import static com.bestsecrets.anagram.rest.resource.api.ApiPath.ANAGRAM;
import static com.bestsecrets.anagram.rest.resource.api.ApiPath.DICTIONARY_PATH_PARAM;
import static com.bestsecrets.anagram.rest.resource.api.ApiPath.PHRASE_PATH_PARAM;

@Component
@HATEOAS
@Path(ANAGRAM)
@Produces(ANAGRAMS_JSON)
public class AnagramResource {

    private final AnagramSearchService anagramSearchService;
    private final ExecutorService taskExecutor;

    @Autowired
    public AnagramResource(AnagramSearchService anagramSearchService){
        this.anagramSearchService = anagramSearchService;
        this.taskExecutor = Executors.newCachedThreadPool();
    }

    @GET
    public void lookupAnagrams(@PathParam(DICTIONARY_PATH_PARAM) String dictionary,
                               @PathParam(PHRASE_PATH_PARAM) String phrase,
                               @Suspended AsyncResponse asyncResponse){

        taskExecutor.submit(anagramSearchService.lookupAnagramsAsync(dictionary, phrase, asyncResponse));

        asyncResponse.setTimeoutHandler(asyncResp -> asyncResp.resume(Response.status(Response.Status.SERVICE_UNAVAILABLE)
                .entity("Operation time out.").build()));
        asyncResponse.setTimeout(10, TimeUnit.SECONDS);
    }

    @PUT
    public void modifyAnagram(){
        throw new NotValidException();
    }

    @POST
    public void saveWordInDictionary(){
        throw new NotValidException();
    }

    @DELETE
    public void deleteWord(){
        throw new NotValidException();
    }
}
