package com.bestsecrets.anagram.rest.resource.api;


public class ApiPath {

    public static final String DICTIONARY_PATH_PARAM = "dictionary";
    public static final String PHRASE_PATH_PARAM = "phrase";
    public static final String ANAGRAM = "/anagram/{" + DICTIONARY_PATH_PARAM + "}/{" + PHRASE_PATH_PARAM + "}";

    public static String ANAGRAM(String dictionary, String phrase){
        return ANAGRAM.replace("{" + DICTIONARY_PATH_PARAM + "}", dictionary).replace("{" + PHRASE_PATH_PARAM + "}", phrase);
    }
}
