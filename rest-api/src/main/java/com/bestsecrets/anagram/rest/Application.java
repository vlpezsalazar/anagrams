package com.bestsecrets.anagram.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.bestsecrets.anagram")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}