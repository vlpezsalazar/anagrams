package com.bestsecrets.anagram.rest.service.impl;

import com.bestsecrets.anagram.engine.api.AnagramSearchEngine;
import com.bestsecrets.anagram.rest.service.api.AnagramSearchService;
import com.bestsecrets.anagram.rest.service.api.DictionaryLoaderService;
import com.bestsecrets.anagram.rest.service.tasks.AnagramSearchTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.container.AsyncResponse;

@Service
public class AnagramSearchServiceImpl implements AnagramSearchService{

    private final AnagramSearchEngine anagramSearchEngine;
    private final DictionaryLoaderService dictionaryLoaderService;

    @Autowired
    public AnagramSearchServiceImpl(AnagramSearchEngine anagramSearchEngine, DictionaryLoaderService dictionaryLoaderService){
        this.anagramSearchEngine = anagramSearchEngine;
        this.dictionaryLoaderService = dictionaryLoaderService;
    }

    public Runnable lookupAnagramsAsync(String dictionary, String phrase, AsyncResponse asyncResponse) {
        return new AnagramSearchTask(dictionaryLoaderService, anagramSearchEngine, dictionary, phrase, asyncResponse);
    }

}
