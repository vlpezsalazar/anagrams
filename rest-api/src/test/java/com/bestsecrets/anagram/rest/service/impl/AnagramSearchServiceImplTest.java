package com.bestsecrets.anagram.rest.service.impl;

import com.bestsecrets.anagram.engine.api.AnagramSearchEngine;
import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import com.bestsecrets.anagram.rest.dto.AnagramSetResponse;
import com.bestsecrets.anagram.rest.service.api.AnagramSearchService;
import com.bestsecrets.anagram.rest.service.api.DictionaryLoaderService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.container.AsyncResponse;
import java.util.Arrays;

import static com.bestsecrets.anagram.engine.api.factory.AnagramSearchStateFactory.ALPHABETIC_CHARACTERS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

public class AnagramSearchServiceImplTest {

    private AnagramSearchService anagramSearchService;

    @Mock
    private AnagramSearchEngine anagramSearchEngine;

    @Mock
    private DictionaryLoaderService dictionaryLoaderService;

    @Mock
    private AnagramDictionary anagramDictionary;

    @Mock
    private AsyncResponse asyncResponse;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        anagramSearchService = new AnagramSearchServiceImpl(anagramSearchEngine, dictionaryLoaderService);
    }

    @Test
    public void lookupAnagramsAsync_shouldReturnAnAnagragramSet_whenDictionaryIsFound() throws Exception {
        //given
        final String dictionary = "existingDictionary";
        final String phrase = "a phrase";
        final String[] anagrams = {"hare sap", "serapha"};
        given(dictionaryLoaderService.load(dictionary)).willReturn(anagramDictionary);
        given(anagramSearchEngine.lookForAnagrams(anagramDictionary, phrase, ALPHABETIC_CHARACTERS)).willReturn(Arrays.asList(anagrams));
        Runnable runnable = anagramSearchService.lookupAnagramsAsync(dictionary, phrase, asyncResponse);

        //when
        runnable.run();

        //then
        ArgumentCaptor<AnagramSetResponse> anagramSetResponseCaptor = ArgumentCaptor.forClass(AnagramSetResponse.class);
        verify(asyncResponse).resume(anagramSetResponseCaptor.capture());
        final AnagramSetResponse anagramSetResponse = anagramSetResponseCaptor.getValue();
        assertNotNull("The returned anagramSet was null ", anagramSetResponse);
        assertThat(anagramSetResponse.anagrams(), containsInAnyOrder(anagrams));
    }

}