package com.bestsecrets.anagram.rest.resource;

import com.bestsecrets.anagram.rest.configuration.JerseyTestConfig;
import com.bestsecrets.anagram.rest.dto.AnagramSetResponse;
import com.bestsecrets.anagram.rest.media.AnagramsMediaType;
import com.bestsecrets.anagram.rest.service.api.AnagramSearchService;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.util.Arrays;

import static com.bestsecrets.anagram.rest.resource.api.ApiPath.ANAGRAM;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.SERVICE_UNAVAILABLE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

public class AnagramResourceTest extends JerseyTest {

    @Mock
    private AnagramSearchService anagramSearchService;

    @Override
    protected Application configure() {

        MockitoAnnotations.initMocks(this);
        ApplicationContext context = new AnnotationConfigApplicationContext(JerseyTestConfig.class);
        return new JerseyTestConfig()
                .property("contextConfig", context)
                .register(JacksonFeature.class)
                .register(new AnagramResource(anagramSearchService));

    }

    @Test
    public void lookupAnagrams_shouldReturnAnagramSetResponse_whenItReceivesCorrectParameters() throws Exception {
        //given
        final String dictionary = "dictionary";
        final String phrase = "this is a good phrase";
        final String[] anagrams = {"phrase good this is a", "phrase this is a godo"};
        final ArgumentCaptor<AsyncResponse> argumentCaptor = ArgumentCaptor.forClass(AsyncResponse.class);

        given(anagramSearchService.lookupAnagramsAsync(eq(dictionary), eq(phrase), argumentCaptor.capture())).willReturn(() -> {
            final AsyncResponse asyncResponse = argumentCaptor.getValue();
            asyncResponse.resume(AnagramSetResponse.of(Arrays.asList(anagrams)));
        });

        //when
        final Response response = target(ANAGRAM(dictionary, phrase)).request().accept(AnagramsMediaType.ANAGRAMS_JSON).get();

        //then
        assertThat(response.getStatus(), is(OK.getStatusCode()));
        assertThat(response.readEntity(AnagramSetResponse.class).anagrams(), containsInAnyOrder(anagrams));
    }

    @Test
    public void lookupAnagrams_shouldReturn503_whenTaskTimesOut() throws Exception {
        //given
        final String dictionary = "dictionary";
        final String phrase = "this is a reaaaally loooooong phrase";
        final ArgumentCaptor<AsyncResponse> argumentCaptor = ArgumentCaptor.forClass(AsyncResponse.class);

        given(anagramSearchService.lookupAnagramsAsync(eq(dictionary), eq(phrase), argumentCaptor.capture())).willReturn(() -> {});

        //when
        final Response response = target(ANAGRAM(dictionary, phrase)).request().accept(AnagramsMediaType.ANAGRAMS_JSON).get();

        //then
        assertThat(response.getStatus(), is(SERVICE_UNAVAILABLE.getStatusCode()));
    }

}