package com.bestsecrets.anagram.rest.service.impl;

import com.bestsecrets.anagram.engine.api.factory.AnagramDictionaryFactory;
import com.bestsecrets.anagram.engine.api.model.AnagramDictionary;
import com.bestsecrets.anagram.rest.exceptions.DictionaryNotFoundException;
import com.bestsecrets.anagram.rest.service.api.DictionaryLoaderService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;

import static com.bestsecrets.anagram.rest.service.impl.DictionaryLoaderServiceImpl.SEPARATOR_REGEX;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;

public class DictionaryLoaderServiceImplTest {

    public static final String DICTIONARIES = "classpath:dictionaries/";

    private DictionaryLoaderService dictionaryLoaderService;

    @Mock
    private AnagramDictionaryFactory anagramDictionaryFactory;

    @Mock
    private AnagramDictionary anagramDictionary;

    @Mock
    private ResourceLoader resourceLoader;

    @Mock
    private Resource resource;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        dictionaryLoaderService = new DictionaryLoaderServiceImpl(anagramDictionaryFactory, resourceLoader);
    }

    @Test
    public void load_shouldReturnADictionary_whenTheDictionaryExists() throws Exception {
        //given
        final String dictionaryName = "dictionaryName";
        final InputStream inputStream = new InputStream() {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };
        given(resourceLoader.getResource(DICTIONARIES + dictionaryName)).willReturn(resource);
        given(resource.getInputStream()).willReturn(inputStream);
        given(anagramDictionaryFactory.load(inputStream, SEPARATOR_REGEX)).willReturn(anagramDictionary);

        //when
        final AnagramDictionary dictionary = dictionaryLoaderService.load(dictionaryName);

        //then
        assertNotNull("The returned dictionary was null", dictionary);
    }

    @Test
    public void load_shouldThrowDictionaryNotFoundException_whenTheDictionaryDoesnotExists() throws Exception {
        //given
        final String dictionaryName = "dictionaryName";

        given(resourceLoader.getResource(DICTIONARIES +dictionaryName)).willReturn(resource);
        given(resource.getInputStream()).willThrow(IOException.class);

        //when
        try {
            dictionaryLoaderService.load(dictionaryName);
            fail("An exception of type " + DictionaryNotFoundException.class.getCanonicalName()+ " was expected");
        } catch (DictionaryNotFoundException e) {

            //then
            assertThat(e.getResponse().getStatus(), is(BAD_REQUEST.getStatusCode()));

        }
    }

}