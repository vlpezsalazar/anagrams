You can run the application using ./gradlew bootRun. Or you can build the JAR file using ./gradlew build. Then you can run the JAR file:

java -jar rest-api/build/libs/rest-api-1.0-SNAPSHOT.jar